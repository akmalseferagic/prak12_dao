<?php
include_once 'top.php';
require_once 'classpeserta.php';
?>
<h2>Daftar Peserta</h2>
<?php
$obj = new Peserta();
$rows = $obj->getAll();
?>
<table class="table">
    <thead>
    <tr class="active">
        <th>No</th><th>NO Registrasi</th><th>Nama Lengkap</th>
        <th>Email</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nomor'].'</td>';
        echo '<td>'.$row['namalengkap'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td><a href="view_kegiatan.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kegiatan.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>
